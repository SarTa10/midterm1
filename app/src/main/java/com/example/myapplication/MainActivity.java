package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.example.myapplication.api.models.Data;
import com.example.myapplication.api.models.posts;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        App.getInstance().getRetrofitClient().getApiService().getUsers().enqueue(new Callback<List<posts>>() {

            @Override
            public void onResponse(Call<List<posts>> call, Response<List<posts>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    List<posts> users = response.body();
                    for (int i = 0; i < users.size(); i++) {
                        Log.d("MyData", users.get(i).toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<posts>> call, Throwable t) {

            }

        });

    }
}