package com.example.myapplication.api.models;

public class Data<T> {

    private T data;

    public void Data() {

    }

    public Data(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
