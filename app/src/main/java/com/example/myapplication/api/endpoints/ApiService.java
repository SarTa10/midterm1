package com.example.myapplication.api.endpoints;

import com.example.myapplication.api.models.Data;
import com.example.myapplication.api.models.posts;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {

        @GET("posts")
        Call<List<posts>> getUsers();
}
